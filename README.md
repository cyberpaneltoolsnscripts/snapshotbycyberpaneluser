This amazing tool makes checking an accounts access/domlogs so much easier and helps you find whats getting hammered and so much more.

See it in action below.

```
bash <(curl -s https://gitlab.com/cyberpaneltoolsnscripts/snapshotbycyberpaneluser/-/raw/master/CyberpanelSnapshotByCyberpanelUser.sh || wget -qO - https://gitlab.com/cyberpaneltoolsnscripts/snapshotbycyberpaneluser/-/raw/master/CyberpanelSnapshotByCyberpanelUser.sh) exampleuser;

Web Traffic Stats Check

=============================================================
Apache Dom Logs POST Requests for 06/Feb/2020 for exampleuser
    341 example.com
     28 my.example.com

HTTP Dom Logs GET Requests for 06/Feb/2020 for exampleuser
   2968 my.example.com
   2534 example.com

HTTP Dom Logs Top 10 bot/crawler requests per domain name for 06/Feb/2020
   2964 example.com
   2129 my.example.com

HTTP Dom Logs top ten IPs for 06/Feb/2020 for exampleuser

251  2001:19f0:4001:bdc:5400:2ff:fe3a:8492
13   77.68.112.43
4    208.102.78.10
4    188.163.109.153
3    110.54.137.4
3    103.84.145.141
2    95.30.19.205
2    82.117.211.122
2    72.46.52.181
2    2.139.203.149

Show unique IP's with whois IP, Country,and ISP

 2001:19f0:5001:bdc:5400:2ff:fe3a:8492    | US | AS-CHOOPA, US
 77.68.112.43     | GB | ONEANDONE-AS Brauerstrasse 48, DE
 208.102.78.10    | US | FUSE-NET, US
 188.163.109.153  | UA | KSNET-AS, UA
 110.54.137.4     | PH | GLOBE-MOBILE-5TH-GEN-AS Globe Telecom Inc., PH
 103.84.145.141   | ID | STARNET-AS-ID PT. Cemerlang Multimedia, ID
 95.30.19.205     | RU | SOVAM-AS, RU
 82.117.211.122   | RS | SERBIA-BROADBAND-AS Serbia BroadBand-Srpske Kablovske mreze d.o.o., RS
 72.46.52.181     | US | ALLO-COMM, US
 2.139.203.149    | ES | TELEFONICA_DE_ESPANA, ES

Checking the IPs that Have Hit the Server Most and What Site they were hitting:
2304  example.com     63.143.42.246
1031  my.example.com  81.209.177.145
251   example.com     2001:19f0:5001:bdc:5400:2ff:fe3a:8492
224   my.example.com  141.98.9.222
184   my.example.com  78.46.61.245
163   my.example.com  216.244.66.248
129   my.example.com  77.68.112.43
116   my.example.com  5.9.151.57
97    my.example.com  5.9.156.121
92    example.com     82.117.211.122

Checking the Top Hits Per Site Per IP:
2302  example.com     63.143.42.246   HEAD  /
91    my.example.com  78.46.61.245    GET   /cart.php
52    my.example.com  216.244.66.248  GET   /robots.txt
29    my.example.com  5.9.156.121     GET   /cart.php
28    my.example.com  81.209.177.145  GET   /robots.txt
18    my.example.com  157.55.39.32    GET   /robots.txt
14    my.example.com  77.68.112.43    GET   /
11    example.com     157.55.39.32    GET   /robots.txt
10    my.example.com  77.68.112.43    GET   /clientarea.php
9     example.com     46.118.126.142  GET   /network-download-test-files/
8     example.com     178.115.236.50  GET   /openvpn-server-with-port-forwarding/
7     example.com     157.55.39.77    GET   /wp-content/uploads/2018/08/2018-08-15-19_05_02-Home-https-error-300x18.png
6     example.com     5.188.210.18    GET   /blog/index.php
6     example.com     46.119.171.190  GET   /network-download-test-files/
6     example.com     46.118.124.65   GET   /network-download-test-files/

Apache Dom Logs find the top number of uri's being requested for 06/Feb/2020
89  /wp-admin/admin-ajax.php?action=hustle_module_viewed
9   /contact.php
3   /dologin.php
2   /wp-admin/admin-post.php
2   /?gf_page=upload
1   /x
1   /wp-json/wp/v2/posts/None
1   /wp-cron.php?doing_wp_cron=1581015745.6487360000610351562500
1   /wp-cron.php?doing_wp_cron=1581015502.7103829383850097656250
1   /wp-cron.php?doing_wp_cron=1581015487.7111759185791015625000


View Apache requests per hour for exampleuser
222  00:00
272  01:00
545  02:00
232  03:00
292  04:00
234  05:00
288  06:00
275  07:00
370  08:00
513  09:00
537  10:00
359  11:00
478  12:00
664  13:00
765  14:00
540  15:00
558  16:00
598  17:00
384  18:00
50   19:00

CMS Checks

Wordpress Checks
Wordpress Login Bruteforcing checks for wp-login.php for 06/Feb/2020 for exampleuser
     10 example.com  
      4 my.example.com  

Wordpress Cron wp-cron.php(virtual cron) checks for 06/Feb/2020 for exampleuser
    251 example.com  

Wordpress XMLRPC Attacks checks for xmlrpc.php for 06/Feb/2020 for exampleuser
      4 example.com  
      1 my.example.com  

Wordpress Heartbeat API checks for admin-ajax.php for 06/Feb/2020 for exampleuser
     89 example.com  
     10 my.example.com  
CMS Bruteforce Checks
Drupal Login Bruteforcing checks for user/login/ for 06/Feb/2020 for exampleuser

Magento Login Bruteforcing checks for admin pages /admin_xxxxx/admin/index/index for 06/Feb/2020 for exampleuser

Joomla Login Bruteforcing checks for admin pages /administrator/index.php for 06/Feb/2020 for exampleuser

vBulletin Login Bruteforcing checks for admin pages admincp for 06/Feb/2020 for exampleuser

Opencart Login Bruteforcing checks for admin pages /admin/index.php for 06/Feb/2020 for exampleuser

Prestashop Login Bruteforcing checks for admin pages /adminxxxx /xxxxadmin for 06/Feb/2020 for exampleuser

=============================================================
Apache Dom Logs POST Requests for 05/Feb/2020 for exampleuser
    360 example.com
     17 my.example.com

HTTP Dom Logs GET Requests for 05/Feb/2020 for exampleuser
   3161 example.com
   2106 my.example.com

HTTP Dom Logs Top 10 bot/crawler requests per domain name for 05/Feb/2020
   3470 example.com
   1513 my.example.com

HTTP Dom Logs top ten IPs for 05/Feb/2020 for exampleuser

237  2001:19f0:4001:bdc:5400:2ff:fe3a:8492
14   104.41.202.71
3    99.229.33.147
3    87.102.129.224
3    85.86.127.90
3    35.220.135.88
2    95.216.159.117
2    93.158.203.96
2    65.154.226.101
2    188.117.136.21

Show unique IP's with whois IP, Country,and ISP

 2001:19f0:5001:bdc:5400:2ff:fe3a:8492    | US | AS-CHOOPA, US
 104.41.202.71    | US | MICROSOFT-CORP-MSN-AS-BLOCK, US
 99.229.33.147    | CA | ROGERS-COMMUNICATIONS, CA
 87.102.129.224   | CH | IMPNET-AS, CH
 85.86.127.90     | ES | EUSKALTEL, ES
 35.220.135.88    | US | GOOGLE, US
 95.216.159.117   | DE | HETZNER-AS, DE
 93.158.203.96    | NL | SERVERIUS-AS, NL
 65.154.226.101   | US | PAN0001, US
 188.117.136.21   | PL | TKPSA-AS, PL

Checking the IPs that Have Hit the Server Most and What Site they were hitting:
2544  example.com     63.143.42.246
237   example.com     2001:19f0:5001:bdc:5400:2ff:fe3a:8492
202   my.example.com  178.151.245.174
163   my.example.com  192.151.152.98
160   example.com     104.41.202.71
153   my.example.com  216.244.66.248
111   example.com     84.56.81.100
72    my.example.com  81.19.209.53
71    my.example.com  204.12.208.154
63    example.com     207.46.13.54

Checking the Top Hits Per Site Per IP:
2542  example.com     63.143.42.246    HEAD  /
100   my.example.com  178.151.245.174  GET   /cart.php
81    my.example.com  192.151.152.98   GET   /cart.php
62    my.example.com  216.244.66.248   GET   /robots.txt
31    my.example.com  5.9.97.200       GET   /cart.php
12    example.com     104.41.202.71    GET   /
8     example.com     207.46.13.54     GET   /2016/08/
6     example.com     46.119.91.186    GET   /network-download-test-files/
6     example.com     46.119.171.190   GET   /network-download-test-files/
6     example.com     46.118.124.65    GET   /network-download-test-files/
6     example.com     46.118.121.248   GET   /network-download-test-files/
6     example.com     40.77.167.168    GET   /wp-content/uploads/2018/08/2018-08-15-19_05_02-Home-https-error-300x18.png
6     my.example.com  81.19.209.53     GET   /templates/sixcustom/img/ruler.png
6     my.example.com  157.55.39.32     GET   /robots.txt
6     my.example.com  14.17.70.178     GET   /

Apache Dom Logs find the top number of uri's being requested for 05/Feb/2020
103  /wp-admin/admin-ajax.php?action=hustle_module_viewed
8    /contact.php
3    /serverstatus.php
3    /index.php
2    //wp-admin/admin-post.php
2    //wp-admin/admin-ajax.php
2    /vendor/phpunit/phpunit/src/Util/PHP/eval-stdin.php
2    //?gf_page=upload
1    /wp-cron.php?doing_wp_cron=1580946586.7642550468444824218750
1    /wp-cron.php?doing_wp_cron=1580946327.2649500370025634765625


View Apache requests per hour for exampleuser
233  00:00
330  01:00
261  02:00
209  03:00
255  04:00
271  05:00
252  06:00
289  07:00
447  08:00
364  09:00
311  10:00
268  11:00
369  12:00
532  13:00
525  14:00
379  15:00
339  16:00
256  17:00
307  18:00
460  19:00
428  20:00
354  21:00
468  22:00
284  23:00

CMS Checks

Wordpress Checks
Wordpress Login Bruteforcing checks for wp-login.php for 05/Feb/2020 for exampleuser
     12 example.com  

Wordpress Cron wp-cron.php(virtual cron) checks for 05/Feb/2020 for exampleuser
    237 example.com  

Wordpress XMLRPC Attacks checks for xmlrpc.php for 05/Feb/2020 for exampleuser
      5 example.com  

Wordpress Heartbeat API checks for admin-ajax.php for 05/Feb/2020 for exampleuser
    117 example.com  
CMS Bruteforce Checks
Drupal Login Bruteforcing checks for user/login/ for 05/Feb/2020 for exampleuser

Magento Login Bruteforcing checks for admin pages /admin_xxxxx/admin/index/index for 05/Feb/2020 for exampleuser

Joomla Login Bruteforcing checks for admin pages /administrator/index.php for 05/Feb/2020 for exampleuser

vBulletin Login Bruteforcing checks for admin pages admincp for 05/Feb/2020 for exampleuser

Opencart Login Bruteforcing checks for admin pages /admin/index.php for 05/Feb/2020 for exampleuser
      3 example.com  

Prestashop Login Bruteforcing checks for admin pages /adminxxxx /xxxxadmin for 05/Feb/2020 for exampleuser

=============================================================
Apache Dom Logs POST Requests for 04/Feb/2020 for exampleuser
    417 example.com
     12 my.example.com

HTTP Dom Logs GET Requests for 04/Feb/2020 for exampleuser
   3724 example.com
   2543 my.example.com

HTTP Dom Logs Top 10 bot/crawler requests per domain name for 04/Feb/2020
   1974 example.com
   1887 my.example.com

HTTP Dom Logs top ten IPs for 04/Feb/2020 for exampleuser

250  2001:19f0:4001:bdc:5400:2ff:fe3a:8492
3    103.212.224.228
3    100.36.83.191
2    94.16.121.91
2    73.157.191.81
2    70.177.66.133
2    66.249.70.19
2    66.249.70.17
2    52.71.101.199
2    50.123.75.184

Show unique IP's with whois IP, Country,and ISP

 2001:19f0:4001:bdc:5400:2ff:fe3a:8492    | US | AS-CHOOPA, US
 103.212.224.228  | NZ | GSLNETWORKS-AS-AP GSL Networks Pty LTD, AU
 100.36.83.191    | US | UUNET, US
 94.16.121.91     | DE | NETCUP-AS netcup GmbH, DE
 73.157.191.81    | US | COMCAST-7922, US
 70.177.66.133    | US | ASN-CXA-ALL-CCI-22773-RDC, US
 66.249.70.19     | US | GOOGLE, US
 66.249.70.17     | US | GOOGLE, US
 52.71.101.199    | US | AMAZON-AES, US
 50.123.75.184    | US | NWFBR, US

Checking the IPs that Have Hit the Server Most and What Site they were hitting:
1298  example.com     63.143.42.246
440   my.example.com  176.9.25.107
270   my.example.com  178.63.34.189
250   example.com     2001:19f0:5001:bdc:5400:2ff:fe3a:8492
176   my.example.com  216.244.66.248
83    my.example.com  66.249.70.15
83    my.example.com  185.210.218.99
79    example.com     46.29.13.9
77    my.example.com  148.251.244.137
63    my.example.com  184.15.172.227

Checking the Top Hits Per Site Per IP:
1297  example.com     63.143.42.246    HEAD  /
139   my.example.com  176.9.25.107     GET   /cart.php
68    my.example.com  216.244.66.248   GET   /robots.txt
9     example.com     46.118.112.190   GET   /network-download-test-files/
8     my.example.com  178.63.34.189    GET   /cart.php
7     my.example.com  84.183.121.65    GET   /viewticket.php?tid=134414&c=x02mcbtJ
6     example.com     46.119.171.190   GET   /network-download-test-files/
6     example.com     46.118.124.65    GET   /network-download-test-files/
6     example.com     46.118.121.248   GET   /network-download-test-files/
6     example.com     159.203.93.122   GET   /
6     my.example.com  203.133.169.243  GET   /dl.php?type=d&id=36&language=swedish
6     my.example.com  203.133.169.243  GET   /announcements/21/SSL-updated-on-VPSorSharedorMisc-Hosts.html?id=21&language=dutch
6     my.example.com  122.13.162.26    GET   /
5     example.com     207.46.13.175    GET   /wp-content/uploads/2019/11/image-4-1024x525.png
5     my.example.com  203.133.169.243  GET   /cart.php?a=add&pid=424

Apache Dom Logs find the top number of uri's being requested for 04/Feb/2020
150  /wp-admin/admin-ajax.php?action=hustle_module_viewed
9    /xmlrpc.php
5    /contact.php
4    /serverstatus.php
2    /index.php
2    /dologin.php
2    //
1    /wp-login.php
1    /wp-cron.php?doing_wp_cron=1580860250.9769840240478515625000
1    /wp-cron.php?doing_wp_cron=1580859331.4954779148101806640625


View Apache requests per hour for exampleuser
225  00:00
197  01:00
214  02:00
202  03:00
267  04:00
259  05:00
547  06:00
349  07:00
371  08:00
344  09:00
217  10:00
348  11:00
410  12:00
440  13:00
406  14:00
304  15:00
350  16:00
290  17:00
309  18:00
302  19:00
689  20:00
320  21:00
351  22:00
287  23:00

CMS Checks

Wordpress Checks
Wordpress Login Bruteforcing checks for wp-login.php for 04/Feb/2020 for exampleuser
     25 example.com  

Wordpress Cron wp-cron.php(virtual cron) checks for 04/Feb/2020 for exampleuser
    250 example.com  

Wordpress XMLRPC Attacks checks for xmlrpc.php for 04/Feb/2020 for exampleuser
      9 example.com  

Wordpress Heartbeat API checks for admin-ajax.php for 04/Feb/2020 for exampleuser
    152 example.com  
CMS Bruteforce Checks
Drupal Login Bruteforcing checks for user/login/ for 04/Feb/2020 for exampleuser

Magento Login Bruteforcing checks for admin pages /admin_xxxxx/admin/index/index for 04/Feb/2020 for exampleuser

Joomla Login Bruteforcing checks for admin pages /administrator/index.php for 04/Feb/2020 for exampleuser

vBulletin Login Bruteforcing checks for admin pages admincp for 04/Feb/2020 for exampleuser

Opencart Login Bruteforcing checks for admin pages /admin/index.php for 04/Feb/2020 for exampleuser

Prestashop Login Bruteforcing checks for admin pages /adminxxxx /xxxxadmin for 04/Feb/2020 for exampleuser

=============================================================
Apache Dom Logs POST Requests for 03/Feb/2020 for exampleuser
    515 example.com
     19 my.example.com

HTTP Dom Logs GET Requests for 03/Feb/2020 for exampleuser
   3673 example.com
   1777 my.example.com

HTTP Dom Logs Top 10 bot/crawler requests per domain name for 03/Feb/2020
    981 example.com
    452 my.example.com

HTTP Dom Logs top ten IPs for 03/Feb/2020 for exampleuser

297  example.com
149  2001:19f0:4001:bdc:5400:2ff:fe3a:8492
12   188.213.49.242
7    my.example.com
4    144.202.38.159
3    209.212.197.63
3    200.29.99.86
2    217.243.129.219
2    178.205.142.87
2    128.70.226.228

Show unique IP's with whois IP, Country,and ISP

 2001:19f0:5001:bdc:5400:2ff:fe3a:8492    | US | AS-CHOOPA, US
 188.213.49.242   | RO | PARFUMURI-FEMEI-AS, RO
 144.202.38.159   | US | AS-CHOOPA, US
 209.212.197.63   | MV | DHIRAAGU-MV-AP DHIVEHI RAAJJEYGE GULHUN PLC, MV
 217.243.129.219  | DE | DTAG Internet service provider operations, DE
 178.205.142.87   | RU | TATTELECOM-AS, RU
 128.70.226.228   | RU | CORBINA-AS OJSC _Vimpelcom_, RU

Checking the IPs that Have Hit the Server Most and What Site they were hitting:
3426  example.com     example.com
1156  my.example.com  my.example.com
559   example.com     63.143.42.246
149   example.com     2001:19f0:5001:bdc:5400:2ff:fe3a:8492
63    my.example.com  66.249.70.15
48    example.com     5.173.113.35
47    example.com     185.22.143.230
44    example.com     45.37.56.251
43    example.com     178.205.142.87
43    my.example.com  209.212.197.63

Checking the Top Hits Per Site Per IP:
2244  example.com     example.com     +0000]  GET
1149  my.example.com  my.example.com  +0000]  GET
885   example.com     example.com     +0000]  HEAD
559   example.com     63.143.42.246         HEAD    /
297   example.com     example.com     +0000]  POST
22    my.example.com  216.244.66.248        GET     /robots.txt
7     my.example.com  my.example.com  +0000]  POST
5     my.example.com  128.70.226.228        GET     /contact.php
3     example.com     8.12.16.99            GET     /
3     example.com     71.6.146.185          GET     /favicon.ico
3     example.com     71.6.146.185          GET     /
3     example.com     5.189.169.198         GET     /openvpn-server-with-port-forwarding/
3     example.com     46.229.168.139        GET     /robots.txt
3     example.com     46.118.121.248        GET     /network-download-test-files/
3     example.com     200.29.99.86          POST    /wp-admin/admin-ajax.php?action=hustle_module_viewed

Apache Dom Logs find the top number of uri's being requested for 03/Feb/2020
304  "POST
54   /wp-admin/admin-ajax.php?action=hustle_module_viewed
3    /redactedadmin/supporttickets.php
3    /contact.php
2    /dologin.php
1    /xmlrpc.php
1    /wp-login.php
1    /wp-cron.php?doing_wp_cron=1580774063.3841280937194824218750
1    /wp-cron.php?doing_wp_cron=1580773835.8021790981292724609375
1    /wp-cron.php?doing_wp_cron=1580773413.8249568939208984375000


View Apache requests per hour for exampleuser
425  00:00
191  01:00
233  02:00
171  03:00
231  04:00
249  05:00
296  06:00
242  07:00
552  08:00
244  09:00
431  10:00
485  11:00
336  12:00
297  13:00
316  14:00
327  15:00
370  16:00
265  17:00
343  18:00
275  19:00
214  20:00
378  21:00
306  22:00
252  23:00

CMS Checks

Wordpress Checks
Wordpress Login Bruteforcing checks for wp-login.php for 03/Feb/2020 for exampleuser
     18 example.com  

Wordpress Cron wp-cron.php(virtual cron) checks for 03/Feb/2020 for exampleuser
    371 example.com  

Wordpress XMLRPC Attacks checks for xmlrpc.php for 03/Feb/2020 for exampleuser
      1 example.com  

Wordpress Heartbeat API checks for admin-ajax.php for 03/Feb/2020 for exampleuser
    129 example.com  
CMS Bruteforce Checks
Drupal Login Bruteforcing checks for user/login/ for 03/Feb/2020 for exampleuser

Magento Login Bruteforcing checks for admin pages /admin_xxxxx/admin/index/index for 03/Feb/2020 for exampleuser

Joomla Login Bruteforcing checks for admin pages /administrator/index.php for 03/Feb/2020 for exampleuser

vBulletin Login Bruteforcing checks for admin pages admincp for 03/Feb/2020 for exampleuser

Opencart Login Bruteforcing checks for admin pages /admin/index.php for 03/Feb/2020 for exampleuser

Prestashop Login Bruteforcing checks for admin pages /adminxxxx /xxxxadmin for 03/Feb/2020 for exampleuser

=============================================================
Apache Dom Logs POST Requests for 02/Feb/2020 for exampleuser
    128 example.com
      5 my.example.com

HTTP Dom Logs GET Requests for 02/Feb/2020 for exampleuser
    960 example.com
    590 my.example.com

HTTP Dom Logs Top 10 bot/crawler requests per domain name for 02/Feb/2020
     56 my.example.com
     55 example.com

HTTP Dom Logs top ten IPs for 02/Feb/2020 for exampleuser

128  example.com
5    my.example.com

Show unique IP's with whois IP, Country,and ISP


Checking the IPs that Have Hit the Server Most and What Site they were hitting:
1639  example.com     example.com
595   my.example.com  my.example.com

Checking the Top Hits Per Site Per IP:
960  example.com     example.com     +0000]  GET
590  my.example.com  my.example.com  +0000]  GET
551  example.com     example.com     +0000]  HEAD
128  example.com     example.com     +0000]  POST
5    my.example.com  my.example.com  +0000]  POST

Apache Dom Logs find the top number of uri's being requested for 02/Feb/2020
133  "POST


View Apache requests per hour for exampleuser
171  15:00
302  16:00
519  17:00
269  18:00
228  19:00
168  20:00
168  21:00
220  22:00
189  23:00

CMS Checks

Wordpress Checks
Wordpress Login Bruteforcing checks for wp-login.php for 02/Feb/2020 for exampleuser
     16 example.com  

Wordpress Cron wp-cron.php(virtual cron) checks for 02/Feb/2020 for exampleuser
     99 example.com  

Wordpress XMLRPC Attacks checks for xmlrpc.php for 02/Feb/2020 for exampleuser

Wordpress Heartbeat API checks for admin-ajax.php for 02/Feb/2020 for exampleuser
     29 example.com  
CMS Bruteforce Checks
Drupal Login Bruteforcing checks for user/login/ for 02/Feb/2020 for exampleuser

Magento Login Bruteforcing checks for admin pages /admin_xxxxx/admin/index/index for 02/Feb/2020 for exampleuser
      2 example.com  

Joomla Login Bruteforcing checks for admin pages /administrator/index.php for 02/Feb/2020 for exampleuser
      2 example.com  

vBulletin Login Bruteforcing checks for admin pages admincp for 02/Feb/2020 for exampleuser

Opencart Login Bruteforcing checks for admin pages /admin/index.php for 02/Feb/2020 for exampleuser

Prestashop Login Bruteforcing checks for admin pages /adminxxxx /xxxxadmin for 02/Feb/2020 for exampleuser

=============================================================
Contents have been saved to exampleuser-CyberpanelSnapshot_2020-02-06_14:10:41.txt
```
